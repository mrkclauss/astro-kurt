import React from "react";
import {Helmet} from "react-helmet";

const App = () => {
    return (
        <div className="App">
            <Helmet>
                <meta charSet="utf-8" />
                <title>KC-2112</title>
                <link rel="canonical" href="http://www.kc2112.com" />
            </Helmet>
            <h1>KC-2112 - Coming soon!!!!!</h1>
        </div>
    );
}
export default App;